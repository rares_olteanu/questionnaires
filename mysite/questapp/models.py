
from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class Questionnaire(models.Model):
    user = models.ForeignKey(User)
    test_name = models.CharField(max_length=200)
    test_description = models.CharField(max_length=1000)
    passed_over = models.IntegerField(default=50)
    creation_date = models.DateField(auto_now_add=True)
    questions_per_page = models.IntegerField(default=5)

    def __str__(self):
        return self.test_name

    class Meta:
        ordering = ['-creation_date']

    def get_absolute_url(self):
        return reverse('questapp:detail', kwargs={'pk': self.pk})

    def validate_unique(self, exclude=None):
        qs = Questionnaire.objects.filter(test_name=self.test_name)
        if qs.exists():
            raise ValidationError('Questionnaire with that name already exists')
        super(Questionnaire, self).validate_unique(exclude)

    def save(self, *args, **kwargs):
        self.validate_unique()
        super(Questionnaire, self).save(*args, **kwargs)


class Question(models.Model):
    test = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    question_text = models.CharField(max_length=300)
    page_number = models.IntegerField(default=1)

    def __str__(self):
        return self.question_text

    def get_absolute_url(self):
        return reverse('questapp:detail', kwargs={'pk': self.pk})


class Answers(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_text = models.CharField(max_length=300)
    score = models.IntegerField(default=0)

    def __str__(self):
        return self.answer_text









