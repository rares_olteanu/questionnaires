from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import transaction
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse_lazy
from .models import Questionnaire, Question, Answers
from django.contrib.auth import login, authenticate
from .forms import SignUpForm, QuestionnaireForm, QuestionForm
from django.views.generic import ListView, CreateView, DeleteView, UpdateView, DetailView
from django.db.models import Max
from django.forms import inlineformset_factory
from braces.views import LoginRequiredMixin


from django.forms.utils import ErrorList
from django import forms


class IndexView(ListView):
    model = Questionnaire
    context_object_name = 'questionnaires'
    template_name = 'questapp/index.html'
    paginate_by = 5


class QuestionnaireView(ListView):
    template_name = 'questapp/detail.html'
    success_url = reverse_lazy('questapp:index')

    def get_queryset(self):
        return Question.objects.filter(test=Questionnaire.objects.get(pk=self.kwargs['pk'])).order_by('page_number')

    def get_context_data(self, **kwargs):
        context = super(QuestionnaireView, self).get_context_data(**kwargs)
        questionnaire = Questionnaire.objects.get(pk=self.kwargs['pk'])
        context['questionnaire'] = questionnaire
        questions = Question.objects.filter(test=questionnaire).order_by('page_number')
        paginator = Paginator(questions, questionnaire.questions_per_page)
        page = self.request.GET.get('page')
        try:
            questions = paginator.page(page)
        except PageNotAnInteger:
            questions = paginator.page(1)
        except EmptyPage:
            questions = paginator.page(paginator.num_pages)
        context['questions'] = questions
        return context


def signup(request):

    form = SignUpForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/')

    form = SignUpForm()
    return render(request, 'questapp/signup.html', {'form': form})


class QuestionnaireAddView(LoginRequiredMixin, CreateView):
    model = Questionnaire
    template_name = 'questapp/base_create.html'
    success_url = 'questapp:index'
    form_class = QuestionnaireForm

    def get_success_url(self):
        return reverse_lazy(self.success_url)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(QuestionnaireAddView, self).form_valid(form)


class QuestionnaireDeleteView(LoginRequiredMixin, DeleteView):
    model = Questionnaire
    template_name = 'questapp/confirm_delete.html'
    template_name_suffix = '_confirm_delete'
    success_url = 'questapp:index'

    def get_object(self, queryset=None):
        obj = super(QuestionnaireDeleteView, self).get_object()
        self.questionnaire = get_object_or_404(Questionnaire, pk=obj.pk)
        return obj

    def get_success_url(self):
        return reverse_lazy(self.success_url)


class QuestionnaireChangeView(LoginRequiredMixin, UpdateView):
    model = Questionnaire
    fields = ['test_name', 'test_description', 'passed_over']
    template_name = 'questapp/base_create.html'
    success_url = 'questapp:index'

    def get_success_url(self):
        return reverse_lazy(self.success_url)


class CreateQuestion(LoginRequiredMixin, CreateView):
    template_name = 'questapp/base_create.html'
    model = Question
    form_class = QuestionForm
    success_url = 'questapp:detail'

    def get_success_url(self):
        return reverse_lazy(self.success_url, kwargs={'pk': self.kwargs['questionnaire_id']})

    def form_valid(self, form):
        questionnaire = Questionnaire.objects.get(pk=self.kwargs['questionnaire_id'])
        questions = Question.objects.filter(test=questionnaire)
        form.instance.test = questionnaire

        question_list = list(questions.values_list('question_text', flat=True))
        if self.request.POST.get('question_text') in question_list:
            form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                u'A question with that text for the questionnaire -- {} -- already exists'.format(questionnaire.test_name)
            ])
            return self.form_invalid(form)

        return super(CreateQuestion, self).form_valid(form)


class QuestionView(LoginRequiredMixin, DetailView):
    model = Question
    template_name = 'questapp/question_detail.html'

    def get_context_data(self, **kwargs):
        context = super(QuestionView, self).get_context_data(**kwargs)
        question = Question.objects.get(pk=self.kwargs['pk'])
        answers = Answers.objects.filter(question=question)
        context['question'] = question
        context['answer'] = answers
        return context


class QuestionChangeView(LoginRequiredMixin, UpdateView):
    model = Question
    form_class = QuestionForm
    template_name = 'questapp/base_create.html'
    success_url = 'questapp:question_detail'

    def get_success_url(self):
        return reverse_lazy(self.success_url, kwargs={'pk': self.kwargs['pk']})


class QuestionDeleteView(LoginRequiredMixin, DeleteView):
    model = Question
    template_name = 'questapp/confirm_delete.html'
    template_name_suffix = '_confirm_delete'
    success_url = 'questapp:detail'

    def get_object(self, queryset=None):
        obj = super(QuestionDeleteView, self).get_object()
        self.question = get_object_or_404(Question, pk=obj.pk)
        self.questionnaire = Questionnaire.objects.get(pk=self.question.test.id)
        return obj

    def get_success_url(self):
        return reverse_lazy(self.success_url, kwargs={'pk': self.questionnaire.id})


@login_required
@transaction.atomic
def manage_answers(request, question_id):
    if question_id:
        question = get_object_or_404(Question, pk=question_id)
    else:
        question = Question()

    answers_formset = inlineformset_factory(Question, Answers, fields=['answer_text', 'score'], can_delete=True, extra=1)

    if request.method == "POST":
        formset = answers_formset(request.POST, request.FILES, instance=question)
        if formset.is_valid():
            formset.save()
            return redirect('questapp:question_detail', pk=question_id)
    else:
        formset = answers_formset(instance=question)
    return render(request, 'questapp/manage_answers.html', {
        'formset': formset,
    })


def result(request, questionnaire_id):
    checked = []
    score, max_score, max_possible = 0, 0, 0
    better_answers = []
    questionnaire = get_object_or_404(Questionnaire, pk=questionnaire_id)
    questions = Question.objects.filter(test=questionnaire).order_by('page_number')
    for question in questions:
        answers = Answers.objects.filter(question=question)
        max_score = answers.aggregate(Max('score'))['score__max']
        if max_score is not None:
            max_possible += max_score
            post_question = 'question' + str(question.id)
            if post_question in request.POST:
                answer_id = int(request.POST.get(post_question))
                myanswer = Answers.objects.get(pk=answer_id)
                checked.append(answer_id)
                score += myanswer.score
                if myanswer.score < max_score:
                    better_answer = answers.filter(score=max_score)[0]
                    a = [item for item in better_answers if item[0] == question.page_number]
                    if len(a) == 0 or len(better_answers) == 0:
                        better_answers.append((
                            question.page_number,
                            question.question_text,
                            better_answer.answer_text,
                            better_answer.score
                        ))
        if score < questionnaire.passed_over:
            message_text = 'you failed the test'
        else:
            message_text = 'you passed the test'
    if len(checked) == len(questions):
        return render(request, 'questapp/result.html', {
            'questionnaire': questionnaire,
            'score': str(score),
            'max_score': str(max_possible),
            'better_answers': better_answers,
            'message_text': message_text,
        })
    else:
        return render(request, 'questapp/detail.html', {
                'questions': questions,
                'questionnaire': questionnaire,
                'checked': checked,
                'error_message': 'you need to answer to {} more questions'.format(str(len(questions)-len(checked)))
            })





