# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-03 13:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('questapp', '0005_auto_20171103_1355'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='question',
            unique_together=set([]),
        ),
    ]
