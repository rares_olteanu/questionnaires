# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-01 15:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('questapp', '0003_auto_20171101_1452'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionnaire',
            name='questions_per_page',
            field=models.IntegerField(default=5),
        ),
    ]
