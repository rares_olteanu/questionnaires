from django.conf.urls import url
from django.contrib.auth.views import login, logout
from . import views

app_name = 'questapp'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),

    url(r'^(?P<pk>[0-9]+)/$', views.QuestionnaireView.as_view(), name='detail'),

    url(r'^login/$', login, {'template_name': 'auth/login.html'}, name='login'),

    url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),

    url(r'^signup/$', views.signup, name='signup'),

    url(r'^add_questionnaire/$', views.QuestionnaireAddView.as_view(),  name='create_questionnaire'),

    url(r'^(?P<pk>[0-9]+)/delete_questionnaire/$', views.QuestionnaireDeleteView.as_view(), name='delete_questionnaire'),

    url(r'^(?P<questionnaire_id>[0-9]+)/result/$', views.result, name='result'),

    url(r'^(?P<questionnaire_id>[0-9]+)/add_question/$', views.CreateQuestion.as_view(), name='add_question'),

    url(r'^(?P<question_id>[0-9]+)/manage_answers/$', views.manage_answers, name='manage_answers'),

    url(r'^(?P<pk>[0-9]+)/edit_questionnaire/$', views.QuestionnaireChangeView.as_view(), name='edit_questionnaire'),

    url(r'^(?P<pk>[0-9]+)/question_detail/$', views.QuestionView.as_view(), name='question_detail'),

    url(r'^(?P<pk>[0-9]+)/edit_question/$', views.QuestionChangeView.as_view(), name='edit_question'),

    url(r'^(?P<pk>[0-9]+)/delete_question/$', views.QuestionDeleteView.as_view(), name='delete_question'),

]
