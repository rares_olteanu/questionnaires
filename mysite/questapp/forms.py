from django.contrib.auth.models import User
from .models import Questionnaire, Answers, Question
from django import forms


class SignUpForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password']


class QuestionnaireForm(forms.ModelForm):
    test_name = forms.CharField(widget=forms.TextInput(attrs={'size': 80}))
    test_description = forms.CharField(widget=forms.TextInput(attrs={'size': 80}))

    class Meta:
        model = Questionnaire
        fields = ['test_name', 'test_description', 'passed_over']


class QuestionForm(forms.ModelForm):
    question_text = forms.CharField(widget=forms.TextInput(attrs={'size': 80}))

    class Meta:
        model = Question
        fields = ['question_text', ]


class AnswersForm(forms.ModelForm):
    class Meta:
        model = Answers
        fields = ['answer_text', 'score']




