
from django.conf.urls import url, include
from django.contrib import admin

app_name = 'questapp'

urlpatterns = [
    url(r'', include('questapp.urls')),
    url(r'^admin/', admin.site.urls),
]
