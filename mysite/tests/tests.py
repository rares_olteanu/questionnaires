import pytest
from django.http import HttpResponse
from django.utils import timezone
from questapp.models import Questionnaire, Question, Answers


@pytest.fixture
def client(request, client):
    func = client.request

    def wrapper(**kwargs):
        print('>>>>', ' '.join('{}={!r}'.format(*item) for item in kwargs.items()))
        resp = func(**kwargs)
        print('<<<<', resp, resp.content)
        return resp

    client.request = wrapper
    return client


@pytest.fixture
def authed_client(db, client, admin_user):
    client.force_login(admin_user)
    return client


@pytest.fixture
def questionnaires(request, db, admin_user):
    return Questionnaire.objects.bulk_create([
        Questionnaire(user=admin_user, test_name='abc', test_description='ABC', passed_over=50,
                                     creation_date=timezone.now(), questions_per_page=5),
        Questionnaire(user=admin_user, test_name='foo', test_description='FOO', passed_over=60,
                                     creation_date=- timezone.timedelta(days=1), questions_per_page=4),
        Questionnaire(user=admin_user, test_name='bar', test_description='BAR', passed_over=70,
                                     creation_date=- timezone.timedelta(days=2), questions_per_page=6),
    ])


@pytest.fixture
def questions(request, db, questionnaires):
    return Question.objects.bulk_create([
        Question(test=questionnaires[0], question_text='what is foo', page_number=1),
        Question(test=questionnaires[0], question_text='what is bar', page_number=2),
        Question(test=questionnaires[2], question_text='foo is bar', page_number=3),
    ])


@pytest.fixture
def answers(request, db, questions):
    return Answers.objects.bulk_create([
        Answers(question=questions[0], answer_text='is foo', score=1),
        Answers(question=questions[1], answer_text='is bar', score=2),
        Answers(question=questions[2], answer_text='is foobar', score=3),
    ])


def test_questionnaire_str():
    assert str(Questionnaire(test_name='foo', test_description='bar', passed_over=3)) == 'foo'


def test_index_view(questionnaires, client):
    resp = client.get('/')

    assert resp.status_code == 200
    assert list(resp.context['questionnaires']) == sorted(questionnaires, key=lambda x: x.creation_date)
    content = resp.content.decode(resp.charset)
    assert 'Add Questionnaire' not in content
    assert 'Home' in content
    assert 'Log Out' not in content
    assert 'Log In' in content
    assert 'Sign Up' in content
    assert 'is_paginated' in resp.context
    for questionnaire in questionnaires:
        assert questionnaire.test_name in content


def test_index_view_empty(db, client):
    resp: HttpResponse = client.get('/')
    assert resp.status_code == 200
    assert len(resp.context['questionnaires']) == 0
    content = resp.content.decode(resp.charset)
    assert 'No questionnaires to show' in content


def test_index_view_authed(db, authed_client):
    resp: HttpResponse = authed_client.get('/')
    content = resp.content.decode(resp.charset)
    assert 'Add Questionnaire' in content
    assert 'Home' in content
    assert 'Log Out' in content
    assert 'Log In' not in content
    assert 'Sign Up' not in content


def test_questionnaire_view(questionnaires, questions, client):
    the_id = questionnaires[0].id
    resp = client.get('/{}/'.format(the_id))
    assert resp.status_code == 200
    assert resp.context['questionnaire'] == questionnaires[0]
    assert 'is_paginated' in resp.context
    assert len(resp.context['questions']) == 2
    assert resp.context['questions'][0].question_text == 'what is foo'
    assert resp.context['questions'][1].question_text == 'what is bar'

    content = resp.content.decode(resp.charset)
    assert 'ABC' in content
    assert 'what is bar' in content
    assert 'is foo' in content
    assert 'Edit Questionnaire' not in content
    assert 'Delete Questionnaire' not in content
    assert 'Add Question' not in content


def test_questionnaire_view_authed(questionnaires, questions, authed_client):
    the_id = questionnaires[0].id
    resp = authed_client.get('/{}/'.format(the_id))
    content = resp.content.decode(resp.charset)
    assert 'Edit Questionnaire' in content
    assert 'Delete Questionnaire' in content
    assert 'Add Question' in content


def test_questionnaire_add_view(db, authed_client, admin_user):
    resp = authed_client.post('/add_questionnaire/', {
        'user': admin_user,
        'test_name': 'foo',
        'test_description': 'bar',
        'passed_over': 50,
        'creation_date': timezone.now(),
        'questions_per_page': 3,
    })

    assert resp.status_code == 302
    assert resp.url == '/'
    resp = authed_client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert 'foo' in content
    assert 'bar' in content


def test_questionnaire_add_view_dupe(questionnaires, authed_client):
    resp = authed_client.post('/add_questionnaire/', {
        'user': questionnaires[0].user,
        'test_name': 'foo',
        'test_description': 'bar',
        'passed_over': 50,
        'creation_date': timezone.now(),
        'questions_per_page': 3,
    })
    content = resp.content.decode(resp.charset)
    assert resp.status_code == 200
    assert 'already exists' in content
    assert len(questionnaires) == 3


@pytest.mark.parametrize('data', [
        {'test_name': '', 'test_description': 'ABC'},
        {'test_name': 'abc', 'test_description': ''},
    ])
def test_questionnaire_add_view_empty(db, authed_client, data):

    resp = authed_client.post('/add_questionnaire/', data)
    content = resp.content.decode(resp.charset)
    assert resp.status_code == 200
    assert 'This field is required' in content


def test_questionnaire_delete_view(questionnaires, authed_client):
    the_id = questionnaires[0].id
    resp = authed_client.get('/{}/delete_questionnaire/'.format(the_id))

    assert resp.status_code == 200
    resp = authed_client.post('/{}/delete_questionnaire/'.format(the_id))
    assert resp.status_code == 302
    assert resp.url == '/'
    assert list(Questionnaire.objects.all()) == questionnaires[1:]


def test_questionnaire_change_view(db, authed_client, questionnaires):
    the_id = questionnaires[0].id
    resp = authed_client.post('/{}/edit_questionnaire/'.format(the_id), {
        'user': questionnaires[0].user,
        'test_name': 'new',
        'test_description': 'NEW',
        'passed_over': 60,
    })

    assert resp.status_code == 302
    assert resp.url == '/'
    assert len(Questionnaire.objects.all()) == 3
    resp = authed_client.get('/')
    content = resp.content.decode(resp.charset)
    assert 'new' in content
    assert 'NEW' in content
    assert 'abc' not in content
    assert 'ABC' not in content
    questionnaire = Questionnaire.objects.get(test_name='new')
    assert questionnaire.passed_over == 60


def test_create_question(questionnaires, authed_client):
    the_id = questionnaires[0].id
    resp = authed_client.post(
        '/{}/add_question/'.format(the_id), {
                              'test': questionnaires[0],
                              'question_text': 'whats_up?',
        })

    assert resp.status_code == 302
    assert resp.url == '/{}/'.format(the_id)
    resp = authed_client.get(resp.url)
    content = resp.content.decode(resp.charset)
    assert "whats_up?" in content


def test_create_question_dupe(questionnaires, questions, authed_client):
    the_id = questionnaires[0].id
    resp = authed_client.post('/{}/add_question/'.format(the_id), {
        'test': questionnaires[0],
        'question_text': 'what is foo',
    })
    content = resp.content.decode(resp.charset)
    assert resp.status_code == 200
    assert 'already exists' in content


def test_question_view(questions, answers, authed_client):
    the_id = questions[0].id
    resp = authed_client.get('/{}/question_detail/'.format(the_id))
    assert resp.status_code == 200
    assert resp.context['question'] == questions[0]

    content = resp.content.decode(resp.charset)
    assert 'what is foo' in content
    assert 'is foo' in content
    assert 'Edit Question' in content
    assert 'Delete Question' in content
    assert 'Manage Answers' in content


def test_question_view_not_authed(questions, answers, client):
    the_id = questions[0].id
    resp = client.get('/{}/question_detail/'.format(the_id))
    assert resp.status_code == 302
    assert resp.url == '/login/?next=/{}/question_detail/'.format(the_id)


def test_question_change_view(db, authed_client, questions):
    the_id = questions[0].id
    resp = authed_client.post('/{}/edit_question/'.format(the_id), {
        'test': questions[0],
        'question_text': 'new_text',
    })

    assert resp.status_code == 302
    assert resp.url == '/{}/question_detail/'.format(the_id)
    assert len(Questionnaire.objects.all()) == 3
    resp = authed_client.get('/{}/question_detail/'.format(the_id))
    content = resp.content.decode(resp.charset)
    assert 'new_text' in content
    question = Question.objects.get(question_text='new_text')
    assert question.id == the_id


def test_question_delete_view(questionnaires, questions, authed_client):
    the_id = questions[0].id
    resp = authed_client.get('/{}/delete_question/'.format(the_id))

    assert resp.status_code == 200
    resp = authed_client.post('/{}/delete_question/'.format(the_id))
    assert resp.status_code == 302
    assert resp.url == '/{}/'.format(questionnaires[0].id)
    assert list(Question.objects.all()) == questions[1:]


def test_manage_answers(questions, answers, authed_client):
    resp = authed_client.get('/{}/manage_answers/'.format(questions[0].id))
    assert resp.status_code == 200
    data = {
        'answers_set-TOTAL_FORMS': 2,
        'answers_set-INITIAL_FORMS': 0,
        'answers_set-MIN_NUM_FORMS': 0,
        'answers_set-MAX_NUM_FORMS': 1000,
        'answers_set-0-answer_text': 'answer1',
        'answers_set-0-score': 30,
        'answers_set-0-id': 50,
        'answers_set-0-question': questions[0],
    }
    resp = authed_client.post('/{}/manage_answers/'.format(questions[0].id), data)
    assert resp.status_code == 200
    content = resp.content.decode(resp.charset)
    #assert '/{}/question_detail/'.format(questions[0].id) in resp.redirect_chain
    #assert resp.url == '/{}/question_detail/'.format(questions[0].id)
